from __future__ import division
from numpy import linalg
import math
import numpy

#return the matrix stored in the file
def readMatrix(filename):
    file=open(filename,'r')
    mat=[]
    for line in file:
        words = line.split()
        row=[]
        for word in words:
            row.append(float(word))
        mat.append(row)

    file.close()
    return numpy.asmatrix(mat)


#decompose C=GG(T) where 
def Cholesky(C):
    #print 'run Cholesky on {}'.format(C)
    imax,jmax=C.shape
    #imax == jmax
    if imax!=jmax:
        raise NameError('ATA is not symmetric')
    G=numpy.zeros(C.shape,dtype=float)
    n=imax
    for j in range(n):
        G[j,j]=C[j,j]
        for k in range(j):
            G[j,j]=G[j,j]-G[j,k]**2
        G[j,j]=math.sqrt(G[j,j])
        for i in range(j+1,n):
            G[i,j]=C[i,j]
            for k in range(j):
                G[i,j]=G[i,j]-G[i,k]*G[j,k]
            G[i,j]=G[i,j]/G[j,j]
    return numpy.asmatrix(G)
        

#solve the upper triangular linear system: A s.t. Ax=b
def solveUpperTriangular(A,b):
    jmax,imax=A.shape
    #print 'A shape is {}'.format(A)
    x=numpy.zeros((imax,1),dtype=float)
    for j in range(jmax-1,-1,-1):
        x[j,0]=b[j,0]
        for i in range(j+1,imax):
            x[j,0]=x[j,0]-A[j,i]*x[i,0]
        x[j,0]=x[j,0]/A[j,j]
    return numpy.asmatrix(x)

def solveLowerTriangular(A,b):
    jmax,imax=A.shape
    x=numpy.zeros((imax,1),dtype=float)
    for j in range(jmax):
        x[j,0]=b[j,0]
        for i in range(j):
            x[j,0]=x[j,0]-A[j,i]*x[i,0]
        x[j,0]=x[j,0]/A[j,j]

    return numpy.asmatrix(x)


def householderQR(A):
    jmax,imax=A.shape
    Q=numpy.eye(jmax,jmax,dtype=float)
    R=numpy.eye(jmax,imax,dtype=float)

    #apply imax times
    for i in range(imax):
        x=numpy.matrix(A[i:, i])
        #print 'x is {}'.format(x)
        norm=linalg.norm(x,ord=2)
        v=x
        if x[0,0]>0:
            v[0,0]=v[0,0]+norm
        else:
            v[0,0]=v[0,0]-norm
        #print 'v is {}'.format(v)
        outer=v*v.T
        #print 'outer of v is {}'.format(outer)
        inner=v.T*v
        inner=inner[0,0]
        #print 'inner of v is {}'.format(inner)
        #print 'outer has shape of {}'.format(outer.shape)
        Hp=numpy.eye(outer.shape[0],outer.shape[1],dtype=float)
        Hp=Hp-outer*(2/inner)
        if i==0:
            H=Hp
        else:#i>=1, block should be H and an Indentity
            H=numpy.eye(jmax,jmax)
            H[i:,i:]=Hp
        A=H*A
        #print 'A becomes {}'.format(A)
        Q=H.T*Q
    return (Q.T,A)
