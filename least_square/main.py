from helper import *
from numpy import linalg
import numpy
import corner

def lsdefault(A,b):
    print '*********run default least square******'
    print 'solution is {}'.format(linalg.lstsq(A,b)[0])

#solve A(T)Ax-A(T)b=0
def lsnormal(A,b):
    print '*********trying normal method*************'
    ATb=A.T*b
    ATA=A.T*A
    print 'A(T)b is {}'.format(ATb)
    #step 1: Cholesky Decomposition
    G=Cholesky(ATA)
    print 'G is {}'.format(G)
    print 'builtin Cholesky gives {}'.format(linalg.cholesky(ATA))
    #print 'Examing: GG(T) is {}'.format(G*G.T)
    #step 2: solve Gy=A(T)b
    print 'now solve Gy=A(T)b'
    y=solveLowerTriangular(G,ATb)
    print 'y is {}'.format(y)
    #print 'Builtin inverse: y is {}'.format(linalg.inv(G)*ATb)
    #step 3: solve G(T)x=y, x is the solution
    print 'now solve G(T)x=y'
    x=solveUpperTriangular(G.T,y)
    #print 'x is {}'.format(x)
    return x

def lshouse(A,b):
    print '******* trying householder *********'
    print 'first do QR decomposition'
    Q,R=householderQR(A)
    #print 'my Q is {}'.format(Q)
    #print 'my R is {}'.format(R)
    #print 'builtin has {} '.format(linalg.qr(A))
    #solution is |Ax-b|=|Q(T)Ax-Q(T)b|=|Rx-Q(T)b|
    QTb=Q.T*b
    print 'Q(T)b is {}'.format(QTb)
    sq=R.shape[1]
    #print 'size is {}'.format(sq)
    x=solveUpperTriangular(R[0:sq,0:sq], numpy.asarray(QTb[0:sq,:]))
    #print 'solution is {}'.format(x)
    return x
#below is the main

print "read the matrix stored in input"
print "make sure A.in and b.in exist"

A=readMatrix('input/A.in')
b=readMatrix('input/b.in')

#print 'A is {}'.format(A)
#print 'b is {}'.format(b)


xnormal=lsnormal(A,b)
xhouse=lshouse(A,b)
lsdefault(A,b)
print 'normal has solution {}'.format(xnormal)
print 'householder has solution {}'.format(xhouse)

print 'the exact solution is {}'.format(corner.x)
print 'condition of A is {}'.format(linalg.cond(A))

print 'difference between house and normal is {}'.format(xnormal-xhouse)
print 'xnormal-xexact has norm {}'.format(linalg.norm(xnormal-corner.x))
print 'xhouse-xexact has norm {}'.format(linalg.norm(xhouse-corner.x))
'''
bad of normal equation: information might be lost from A=>ATA
  sensitivity is worsened as K(ATA) = K(A)^2

bad of householder:
  on m>>n, the computation is much more
  
'''
