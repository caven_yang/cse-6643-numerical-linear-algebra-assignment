import numpy
from numpy import linalg
import math

k=0.0000001
A=[[1,1],[k,0],[0,k]]
A=numpy.asmatrix(A)
I=numpy.eye(3,2)
I=numpy.asmatrix(I)
Z=numpy.zeros((3,2))
Z=numpy.asmatrix(Z)

C=numpy.eye(12,6)
C=numpy.asmatrix(C)

C[0:3,0:2]=A
C[3:6,0:2]=Z
C[6:9,0:2]=Z
C[9:, 0:2]=I

C[0:3,2:4]=Z
C[3:6,2:4]=A
C[6:9,2:4]=Z
C[9:12,2:4]=A

C[0:3,4:6]=Z
C[3:6,4:6]=Z
C[6:9,4:6]=A
C[9:12,4:6]=I

for i in range(12):
    for j in range(6):
        print str(C[i,j])+' ',
    print ''

a=1/math.sqrt(6)
x=[a,a,a,a,a,a]
x=numpy.asmatrix(x)
x=x.T


b=C*x



for i in range(12):
    print b[i,0]
