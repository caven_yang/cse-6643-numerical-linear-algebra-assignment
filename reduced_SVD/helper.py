from __future__ import division
from numpy import linalg
import math
import numpy


def householderQR(A):
    jmax,imax=A.shape
    Q=numpy.eye(jmax,jmax,dtype=float)
    R=numpy.eye(jmax,imax,dtype=float)

    #apply imax times
    for i in range(imax):
        x=numpy.matrix(A[i:, i])
        #print 'x is {}'.format(x)
        norm=linalg.norm(x,ord=2)
        v=x
        if x[0,0]>0:
            v[0,0]=v[0,0]+norm
        else:
            v[0,0]=v[0,0]-norm
        #print 'v is {}'.format(v)
        outer=v*v.T
        #print 'outer of v is {}'.format(outer)
        inner=v.T*v
        inner=inner[0,0]
        #print 'inner of v is {}'.format(inner)
        #print 'outer has shape of {}'.format(outer.shape)
        Hp=numpy.eye(outer.shape[0],outer.shape[1],dtype=float)
        Hp=Hp-outer*(2/inner)
        if i==0:
            H=Hp
        else:#i>=1, block should be H and an Indentity
            H=numpy.eye(jmax,jmax)
            H[i:,i:]=Hp
        A=H*A
        #print 'A becomes {}'.format(A)
        Q=H.T*Q
    return (Q.T,A)


def testMatrix(m,n):
    mat=numpy.zeros((m,n))
    mat=numpy.asmatrix(mat)
    for i in range(m):
        for j in range(n):
            mat[i,j]=1/(i+1*2*(j+1)-1)
    return mat

def symSchur2(A,p,q):
    if A[p,q] != 0:
        tao=(A[q,q]-A[p,p])/(2*A[p,q])
        t=math.sqrt(1+tao*tao)
        if tao >= 0:
            t=1/(tao+t)
        else:
            t=1/(tao-t)
        c=1/math.sqrt(1+t*t)
        s=t*c
    else:
        c,s=1,0
    return c,s


def Jacobi(n,p,q,c,s):
    J=numpy.eye(n)
    J[p,p]=c
    J[p,q]=s
    J[q,p]=-s
    J[q,q]=c
    return numpy.asmatrix(J)




def solve_22(A):
    Su=A*A.T
    tan=(Su[0,1]+Su[1,0])/(Su[0,0]-Su[1,1])
    phi=0.5*numpy.arctan(tan)
    cphi=math.cos(phi)
    sphi=math.sin(phi)
    U=numpy.zeros((2,1))
    U=[[cphi,-sphi],[sphi,cphi]]
    U=numpy.asmatrix(U)
    Sw=A.T*A
    tan=(Sw[0,1]+Sw[1,0])/(Sw[0,0]-Sw[1,1])
    theta=0.5*numpy.arctan(tan)
    ctheta=math.cos(theta)
    stheta=math.sin(theta)
    W=[[ctheta,-stheta],[stheta,ctheta]]
    W=numpy.asmatrix(W)
    
    return U,U.T*A*W,W

