from __future__ import division
from numpy import linalg
import math
import numpy
from helper import *

m,n=6,5
A=testMatrix(m,n)
print A
'''
Q,R=householderQR(A)
R0=R[0:n,:]
U0,S0,V0=linalg.svd(R0)
R[0:n,:]=U0
S0=numpy.diag(S0)
Q=Q*R
#checked Q is orthogonal
print Q
print S0
print V0
'''

C=A.T*A
n=C.shape[0]
V=numpy.asmatrix(numpy.eye(n))
for iter in range(6):
    for p in range(n):
        for q in range(p+1,n):
            c,s=symSchur2(C,p,q)
            J=Jacobi(n,p,q,c,s)
            C=J.T*C*J
            V=V*J

if linalg.norm(V.T*V-numpy.eye(n),ord='fro') < 0.001:
    print '\nA.T*A=V*SIGMA*V.T'
    print linalg.norm(A.T*A-V*C*V.T,ord='fro')
    print 'SIGMA'
    print C

D=A*A.T
n=D.shape[0]
U=numpy.asmatrix(numpy.eye(n))

for iter in range(10):
    for p in range(n):
        for q in range(p+1,n):
            c,s=symSchur2(D,p,q)
            J=Jacobi(n,p,q,c,s)
            D=J.T*D*J
            U=U*J

if linalg.norm(U.T*U-numpy.eye(n),ord='fro') < 0.001:
    print '\nA*A.T=U*SIGMA*U.T'
    print linalg.norm(A*A.T-U*D*U.T,ord='fro')
    print '\nSIGMA'
    print D

print linalg.norm(D[0:5,0:5]-C,ord='fro')

SIGMA=U.T*A*V


print '\nU'
print U

print '\nV'
print V
print '\naccuracy: A-U*SIGMA*V.T'
print linalg.norm(A-U*SIGMA*V.T)


print '\nSIGMA: diagonal'
print numpy.diagonal(SIGMA)
print '\nall'
print SIGMA

